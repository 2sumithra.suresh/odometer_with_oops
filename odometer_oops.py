import sys


def is_ascending(n: int) -> bool:
    s = str(n)
    return all(a < b for a, b in zip(s, s[1:]))


class Odometer:
    def __init__(self, cur_reading):
        self.size = len(str(cur_reading))
        self.start = int("123456789"[:self.size])
        self.end = int("123456789"[-self.size:])
        self.reading = cur_reading

    def __str__(self) -> str:
        return str(self.reading)

    def __repr__(self) -> str:
        return f"{self.start} <<<< {self.reading} >>> {self.end}"

    def forward(self, steps: int = 1):
        for _ in range(steps):
            if self.reading == self.end:
                self.reading = self.start
            else:
                self.reading += 1
                while not is_ascending(self.reading):
                    self.reading += 1
        return self.reading

    def backward(self, steps: int = 1):
        for _ in range(steps):
            if self.reading == self.start:
                self.reading = self.end
            else:
                self.reading -= 1
                while not is_ascending(self.reading):
                    self.reading -= 1
        return self.reading

    def distance(self, next_reading: int) -> int:
        current = self.reading
        distance = 0

        while current != next_reading:
            if current == self.end:
                current = self.start
            else:
                current += 1
                while not is_ascending(current):
                    current += 1
            distance += 1
        return distance


for arg in sys.argv[1:]:
    Oo = Odometer(int(arg))
    print(f"{Oo}\n{Oo.backward()}\n{Oo.forward()}")

odometer1 = Odometer(123)
print(odometer1.distance(235))
