Creating an Odometer class and odometer object to solve the problem

1. Forward Function - Gives the next reading of Odometer

2. Backward Function - Gives previous reading of Odometer

3. Distance Function - Gives the number of steps between two odometer readings


ROLES

1. Sumithra - Creating README, Initializing Odometer class, Forward Function

2. Akshaya - Backward Function, Distance Function
